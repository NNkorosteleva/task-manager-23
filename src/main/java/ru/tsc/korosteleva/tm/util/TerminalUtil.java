package ru.tsc.korosteleva.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.exception.field.NumberIncorrectException;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new NumberIncorrectException(value);
        }
    }

    @Nullable
    static Date nextDate() {
        final String value = nextLine();
        return DateUtil.toDate(value);
    }

}
