package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;

public interface IAuthRepository {

    void setUserId(@NotNull String userid);

    @NotNull
    String getUserId();

}
