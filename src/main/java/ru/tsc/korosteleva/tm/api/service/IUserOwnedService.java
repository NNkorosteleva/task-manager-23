package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.api.repository.IUserOwnedRepository;
import ru.tsc.korosteleva.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

}
