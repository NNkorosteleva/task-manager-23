package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description,
                   @Nullable Date dateBegin,
                   @Nullable Date dateEnd);

    @NotNull
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @Nullable String description);

    @NotNull
    Project removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}