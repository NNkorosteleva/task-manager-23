package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login,
                @Nullable String password,
                @Nullable String email,
                @Nullable String role);

    @NotNull
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User findOneByLogin(@Nullable String login);

    @NotNull
    User findOneByEmail(@Nullable String email);

    @NotNull
    User removeById(@Nullable String id);

    @NotNull
    User removeByLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
