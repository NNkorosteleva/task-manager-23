package ru.tsc.korosteleva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IAuthRepository;
import ru.tsc.korosteleva.tm.api.service.IAuthService;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.entity.UserNotFoundException;
import ru.tsc.korosteleva.tm.exception.user.*;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IAuthRepository authRepository;

    public AuthService(@NotNull final IUserService userService,
                       @NotNull final IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        final User user = Optional.ofNullable(userService.findOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        String hash = HashUtil.salt(password);
        Optional.of(user).filter(u -> !user.getLocked())
                .orElseThrow(UserLockedException::new);
        Optional.of(user).filter(u -> hash.equals(user.getPasswordHash()))
                .orElseThrow(LoginPasswordIncorrectException::new);
        authRepository.setUserId(user.getId());
    }

    @Override
    public void logout() {
        authRepository.setUserId(null);
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login,
                         @NotNull final String password,
                         @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User getUser() {
        final String userId = getUserId();
        return Optional.ofNullable(userService.findOneById(userId))
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public String getUserId() {
        return Optional.ofNullable(authRepository.getUserId())
                .orElseThrow(AccessDeniedException::new);
    }

    @Override
    public boolean isAuth() {
        return authRepository.getUserId() != null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = Optional.ofNullable(user.getRole()).orElseThrow(PermissionException::new);
        Arrays.stream(Stream.of(roles).toArray())
                .filter(hasRole -> hasRole.equals(role))
                .findFirst()
                .orElseThrow(PermissionException::new);
    }

}
