package ru.tsc.korosteleva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.entity.UserNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.IndexIncorrectException;
import ru.tsc.korosteleva.tm.exception.user.*;
import ru.tsc.korosteleva.tm.model.User;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @Nullable final String email) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        Optional.of(login).filter(l -> !isLoginExist(login))
                .orElseThrow(() -> new LoginExistException(login));
        Optional.of(email).filter(e -> !isEmailExist(email))
                .orElseThrow(() -> new EmailExistException(email));
        return repository.create(login, password, email);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @Nullable final String email,
                       @Nullable final String role) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        Optional.of(login).filter(l -> !isLoginExist(login))
                .orElseThrow(() -> new LoginExistException(login));
        Optional.of(email).filter(e -> !isEmailExist(email))
                .orElseThrow(() -> new EmailExistException(email));
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        Optional.of(role).filter(r -> !isRoleIncorrect(role))
                .orElseThrow(() -> new RoleIncorrectException(role));
        return repository.create(login, password, email, role);
    }

    @NotNull
    @Override
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return Optional.ofNullable(repository.updateUser(id, firstName, lastName, middleName))
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        return Optional.ofNullable(repository.setPassword(id, password))
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User findOneByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return repository.findOneByLogin(login);
    }

    @NotNull
    @Override
    public User findOneByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        return repository.findOneByEmail(email);
    }

    @NotNull
    @Override
    public User removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return Optional.ofNullable(repository.removeById(id))
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return Optional.ofNullable(repository.removeByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        repository.lockUserByLogin(login);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        repository.unlockUserByLogin(login);
    }

    private boolean isEmailExist(@NotNull final String email) {
        return findOneByEmail(email) != null;
    }

    private boolean isLoginExist(@NotNull final String login) {
        return findOneByLogin(login) != null;
    }

    private boolean isRoleIncorrect(@NotNull final String role) {
        return Role.toRole(role) == null;
    }

}
