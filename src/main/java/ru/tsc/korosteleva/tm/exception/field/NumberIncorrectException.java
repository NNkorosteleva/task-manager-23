package ru.tsc.korosteleva.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect.");
    }

    public NumberIncorrectException(String value) {
        super("Error! Number ``" + value + "`` is incorrect.");
    }

}
