package ru.tsc.korosteleva.tm.exception.user;

public final class EmailExistException extends AbstractUserException {

    public EmailExistException(String value) {
        super("Error! Email ``" + value + "`` is exist.");
    }

}
