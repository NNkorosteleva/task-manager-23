package ru.tsc.korosteleva.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    @NotNull
    private String userId;

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @NotNull
    @Override
    public String getUserId() {
        return userId;
    }

}
