package ru.tsc.korosteleva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IRepository;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    @Override
    public M add(final @NotNull M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(final @NotNull Comparator<M> comparator) {
        return records.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(final @NotNull Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(final @NotNull String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(final @NotNull Integer index) {
        return records.get(index);
    }

    @Nullable
    @Override
    public M remove(final @NotNull M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(final @NotNull String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(final @NotNull Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public void removeAll(final @NotNull Collection<M> collection) {
        records.removeAll(collection);
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public long getSize() {
        return records.size();
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        return findOneById(id) != null;
    }

}
