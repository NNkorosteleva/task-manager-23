package ru.tsc.korosteleva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.api.service.ICommandService;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
