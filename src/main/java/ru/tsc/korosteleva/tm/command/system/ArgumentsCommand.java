package ru.tsc.korosteleva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
