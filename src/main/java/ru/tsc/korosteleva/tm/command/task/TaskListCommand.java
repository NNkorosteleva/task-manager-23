package ru.tsc.korosteleva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.model.Task;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}
